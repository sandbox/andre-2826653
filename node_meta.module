<?php

/**
 * Implements hook_form_node_form_alter().
 */
function node_meta_form_node_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {

  $node = $form_state->getFormObject()->getEntity();
  $defaults = node_meta_get_tags($node);

  $form['node_meta'] = array(
    '#type' => 'details',
    '#title' => t('Meta tags'),
    '#group' => 'advanced',
    '#attributes' => array(
      'class' => array('node-form-options'),
    ),
    '#weight' => 85,
    '#optional' => TRUE,
  );

  $form['meta_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Meta title'),
    '#required' => FALSE,
    '#default_value' => isset($defaults['meta_title']) ? $defaults['meta_title'] : '',
    '#group' => 'node_meta',
    '#description' => 'The title-tag value, defaults to node title if empty',
  );

  $form['meta_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Meta description'),
    '#required' => FALSE,
    '#default_value' => isset($defaults['meta_description']) ? $defaults['meta_description'] : '',
    '#group' => 'node_meta',
    '#description' => 'The meta description, defaults to summary if empty.',
  );

  $form['meta_keywords'] = array(
    '#type' => 'textfield',
    '#title' => t('Meta keywords'),
    '#required' => FALSE,
    '#default_value' => isset($defaults['meta_keywords']) ? $defaults['meta_keywords'] : '',
    '#group' => 'node_meta',
    '#description' => 'Meta keywords, nowadays ignored by all respectable search engines. No default',
  );

  foreach (array_keys($form['actions']) as $action) {
    if ($action != 'preview' && isset($form['actions'][$action]['#type']) && $form['actions'][$action]['#type'] === 'submit') {
      $form['actions'][$action]['#submit'][] = 'node_meta_save_tags';
    }
  }

}

function node_meta_get_tags(Drupal\Core\Entity\EntityInterface $entity) {
    $db = db_select('node_meta_tags', 'n');
    $db->fields('n', array('meta_description', 'meta_title', 'meta_keywords'))
      ->condition('n.nid', $entity->id())
      ->condition('n.vid', $entity->vid->value);
    $result = $db->execute()->fetchAssoc();
  return $result;
}

function node_meta_entity_save_tags(Drupal\Core\Entity\EntityInterface $entity, $values) {

  if ($entity->getEntityType() === 'node') {
    return;
  }

  $table_name = 'node_meta_tags';

  $db = db_select($table_name, 'n');
  $db->fields('n', array('nid'))
    ->condition('n.nid', $entity->id())
    ->condition('n.vid', $entity->vid->value);

  $result = $db->execute()->fetchAll();

  if (count($result) > 0) {
    $db = db_update($table_name);
    $db->condition('nid', $entity->id());
    $db->condition('vid', $entity->vid->value);
  }
  else {
    $db = db_insert($table_name);
  }

  $db->fields(array(
    'nid' => $entity->id(),
    'vid' => $entity->vid->value,
    'meta_description' => $values['meta_description'],
    'meta_title' => $values['meta_title'],
    'meta_keywords' => $values['meta_keywords'],
  ))->execute();

}

function node_meta_save_tags($form, Drupal\Core\Form\FormState $form_state) {
  $node = $form_state->getFormObject()->getEntity();
  node_meta_entity_save_tags($node, $form_state->getValues());
}

/**
 * Implements hook_page_attachments().
 */
function node_meta_page_attachments(&$attachments) {
  if ($node = \Drupal::routeMatch()->getParameter('node')) {
    if (!is_object($node)) {
      return;
    }

    $tags = node_meta_get_tags($node);

    if(!isset($tags['meta_description']) && isset($node->body)) {
      $tags['meta_description'] = trim(preg_replace('/\s\s+/', ' ', strip_tags($node->body->summary ? $node->body->summary : text_summary($node->body->value))));
    }

    if (!$tags || count($tags) === 0) {
      return;
    }
    foreach ($tags as $tag_name => $tag_value) {
      if ($tag_value && $tag_name !== 'meta_title') {
        $attachment = array(
          '#type' => 'html_tag',
          '#tag' => 'meta',
          '#attributes' => array(
            'name' => str_replace('meta_', '', $tag_name),
            'content' => $tag_value,
          ),
        );

        if ($tag_name === 'meta_title') {
          $attachment['#tag'] = 'title';
          $attachment['#value'] = $tag_value;
          unset($attachment['#attributes']);
        }

        $attachments['#attached']['html_head'][] = array(
          $attachment,
          str_replace('meta_', '', $tag_name),
        );

      }

    }

  }
}

/**
 * Implements hook_preprocess_html().
 */
function node_meta_preprocess_html(&$vars){
  if ($node = \Drupal::routeMatch()->getParameter('node')) {
    if (!is_object($node)) {
      return;
    }
    $tags = node_meta_get_tags($node);
    if(isset($tags['meta_title']) && $tags['meta_title']) {
      $vars['head_title'] = ['name' => $tags['meta_title']];
    }
  }
}